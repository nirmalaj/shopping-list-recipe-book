import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService{
    recipeSelected = new EventEmitter<Recipe>();
    private recipes: Recipe[] = [
		new Recipe(
            'IceCream- Almond', 
            'This is simply a test recipe',
            'https://bethelsnj.org/wp-content/uploads/2018/06/AMAZING-Vegan-Cherry-Pie-ICE-CREAM-10-ingredients-simple-methods-SO-delicious-vegan-recipe-icecream-dessert-cherry.jpg',
            [
                new Ingredient('Cream',1), 
                new Ingredient('Almonds', 10),
                new Ingredient('Cone', 1) 
            ]
            ),
		new Recipe(
            'Misal bhaji', 
            'This is simply a test recipe',
            'https://www.maxpixel.net/static/photo/1x/Recipe-Soup-Noodle-Curried-Spicy-Chicken-Khaosoi-2344152.jpg',
            [
                new Ingredient('sprouts', 1),
                new Ingredient('onion', 4),
                new Ingredient('potatoes', 5),
                new Ingredient('Coriander', 1)
            ]
            )
    ];

    constructor(private shoppingListService: ShoppingListService){
    }
    
    getRecipes(){
        return this.recipes.slice();
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]){
        this.shoppingListService.addIngredients(ingredients);
    }
}